import React from 'react';
import {WrapperLoading,  BallItemOne, BallItemTwo, BallItemThree} from './style';

const Loading = ({...props}) =>{
    return(
        <WrapperLoading>
            <BallItemOne />
            <BallItemTwo />
            <BallItemThree />            
        </WrapperLoading>
    )
}
export default Loading;