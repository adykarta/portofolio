import styled from 'styled-components';

export const WrapperLoading = styled.div`
    display:flex;
    flex-direction:row;
    align-items:center;
    justify-content:center;
    align-items:center;
    z-index:100000;

`
const BallItem =`
    border:1px solid white;
    border-radius:50%;
    background-color:white;
    margin:0rem 0.5rem;
       
   


`
export const BallItemOne = styled.div`
    ${BallItem}
    animation: mymove 0.8s infinite ease-in-out;
    @keyframes mymove {
        0% {
            width: 0px;
            height:0px;
        }
        50%{
            width:0px;
            height:0px;
        }
        100% {
            width: 20px;
            height:20px;
        
        }
    }
  

`
export const BallItemTwo = styled.div`
    ${BallItem}
    animation: mymove2 0.8s infinite ease-in-out;
  
    @keyframes mymove2 {
        0% {
            width: 0px;
            height:0px;
        }
        50%{
            width:20px;
            height:20px;
        }
        100% {
            width: 0px;
            height:0px;
        
        }
    }
  

`
export const BallItemThree = styled.div`
    ${BallItem}
    animation: mymove3 0.8s infinite ease-in-out;
    @keyframes mymove3 {
        0% {
            width: 20px;
            height:20px;
        }
        50%{
            width:0px;
            height:0px;
        }
        100% {
            width: 0px;
            height:0px;
        
        }
    }
  

`