import styled from 'styled-components';

export const WrapperAbout = styled.div`
    padding:2rem 1rem;   
    min-height:100vh;
    @media only screen and (max-width:450px){
        padding:1rem 1rem;   
    }
`
export const WrapperRight = styled.div`
    position:absolute;
    top:-2rem;
    right:-15rem;
    .rightBlock{
        width:700px;
        height:100vh;
        background-color:#3E3F43;
        border:1px solid #343f43;
        border-bottom-left-radius:70px;
        position:relative;
        img{
            position:absolute;
           
            bottom:3rem;
            width:150%;
            animation: in 1s ;
            animation-fill-mode: forwards;

        }
        @keyframes in {
            0% {
                right:-100rem;
            }
            
            100%{
                right:12rem;
    
    
            }
        }
    }
    @media only screen and (max-width:768px){
        width:500px;
    }
    @media only screen and (max-width:450px){
        width:400px;
        .rightBlock img{
            width:100%;
        }
        @keyframes in {
            0% {
                right:-100rem;
            }
            
            100%{
                right:22rem;
    
            }
        }
    }

`
export const WrapperGreetings = styled.div`
    display:flex;
    flex-direction:column;
    padding-left:5rem;
    padding-top:2rem;
    width:50vw;

    animation: intext 1s ;
    animation-fill-mode: forwards;
    
    @keyframes intext {
        0% {
            opacity:0;
        }
        
        100%{
            opacity:1;
           


        }
    }
    .arrow{
        margin-left:15rem;
        &:hover{
            cursor:pointer;
        }
        animation: movedown 1s infinite ease-in-out;
    }
   
    @keyframes movedown {
        0% {
            margin-top:4rem;
        }
        50% {
            margin-top:8rem;
        
        }
        100%{
            margin-top:4rem;


        }
    }
    @media only screen and (max-width:450px){
        padding-left:1rem;
        padding-top:0rem;
        .arrow{
            margin-left:2rem;
        }
    }



`
export const WrapperSocial = styled.div`
    display:flex;
    flex-direction:row;
    a{
            margin-right:0.5rem;
            &:hover{
                cursor:pointer;
            }
            text-decoration:none;

    }
    
`