import React from 'react';
// import Loading from '../../components/Loading';
import {Link as ScrollLink} from 'react-scroll';
import {WrapperAbout, WrapperRight, WrapperGreetings, WrapperSocial} from './style';
import Ady from '../../assets/img/ady.png';
import {ReactComponent as Whatsapp} from '../../assets/icon/whatsapp.svg';
import {ReactComponent as Linkedin} from '../../assets/icon/linkedin.svg';
import {ReactComponent as Instagram} from '../../assets/icon/instagram.svg';
import {ReactComponent as Github} from '../../assets/icon/github.svg';
import {ReactComponent as Down} from '../../assets/icon/down-arrows.svg';

function About({...props}){
    return(
        <WrapperAbout>
           
            <WrapperRight>
                <div className="rightBlock">
                    <img src={Ady} alt="gambar-ady" />
                </div>        
            </WrapperRight>
            <WrapperGreetings>
                <h1>Hey yo!</h1>
                <h1>I am <span style={{fontWeight:'bolder'}}>Muhamad Istiady Kartadibrata </span></h1>
                <br/>
                <h2>Final year <span style={{fontWeight:'bolder'}}>Computer Science</span> student at <span style={{fontWeight:'bolder'}}>Universitas Indonesia</span></h2>
                <br/>
                <WrapperSocial>
                    <a href="https://wa.me/6281317250076?text=I'm%20interested%20in%20you" target="_blank" rel="noopener noreferrer">
                        <Whatsapp className="icon"/>
                    </a>
                    <a href="https://linkedin.com/in/adykarta" target="_blank" rel="noopener noreferrer">
                        <Linkedin className="icon" />
                    </a>
                    <a href="https://gitlab.com/adykarta" target="_blank" rel="noopener noreferrer">
                        <Github className="icon" />
                    </a>
                    <a href="https://www.instagram.com/adykartaa/" target="_blank" rel="noopener noreferrer">
                        <Instagram className="icon" />
                    </a>
                </WrapperSocial>
                <ScrollLink to="vote" spy={true} smooth={true} duration={500} className="header-item">   
                    <Down className="arrow"/>
                </ScrollLink>
            </WrapperGreetings>


     
        </WrapperAbout>
    )
}
export default About;