import React,{useState, useEffect} from 'react';
import {WrapperVote, WrapperTitle, WrapperButton, ButtonItem, WrapperChart} from './style';
import {Doughnut} from 'react-chartjs-2';
import axios from 'axios';

function Vote({...props}){
    const [data, setData] = useState({
        labels: ['Yes', 'No'],
        total:0,
        datasets: [
          {
            label: 'Impressed',
            backgroundColor: [
              '#B21F00',
              '#C9DE00',
          
            ],
            hoverBackgroundColor: [
            '#501800',
            '#4B5000',
            
            ],
            data: [65, 59]
          }
        ]
    })
   
    const [isVoted, setIsVoted] = useState(window.localStorage.getItem('isVoted') ?? false)
   
    const fetchData=()=>{
        axios.get('https://portofolio-adykarta.herokuapp.com/api/vote',{headers:{'Access-Control-Allow-Origin': '*'}})
        .then((res)=>{
           
            let updated = {
                labels: ['Yes', 'No'],
                datasets: [
                  {
                    label: 'Impressed',
                    backgroundColor: [
                      '#B21F00',
                      '#C9DE00',
                  
                    ],
                    hoverBackgroundColor: [
                    '#501800',
                    '#4B5000',
                    
                    ],
                    total:res.data[0].total,
                    data: [res.data[0].good, res.data[0].bad]
                  }
                ]

            }        
            setData(updated)    
        })

    }
    useEffect(()=>{
      fetchData();
    },[isVoted])
  
    const handleClick = async(type)=>{
        const form = {
            'type':type
        }
        try{
            await axios.post('https://portofolio-adykarta.herokuapp.com/api/vote',form,{headers:{'Access-Control-Allow-Origin': '*'}})
            fetchData();
            window.localStorage.setItem('isVoted', true);
            setIsVoted(true);
        }
        catch(err){
            console.log(err)
        }
    }
    return(
        <WrapperVote>
            <WrapperTitle>
                <h1>ARE U IMPRESSED YET? xD</h1>
            </WrapperTitle>
            <h2>Total Vote: {data.datasets[0].total}</h2>
            {
                isVoted ?
                <WrapperButton>
                    <h1>Thank you for your vote!</h1>
                </WrapperButton>
                :
                <WrapperButton>

                <ButtonItem onClick={()=>handleClick('bad')}>
                    <h2>big NO!</h2>                  
                </ButtonItem>
                <ButtonItem onClick={()=>handleClick('good')}>
                    <h2>oh YES!</h2>           
                </ButtonItem>         
            </WrapperButton>


            }
           
            <WrapperChart>
                <Doughnut
                    data={data}
                    options={{
                        title:{
                        display:false,
                        text:'Impression',
                        fontSize:40
                        },
                        legend:{
                        display:true,
                        position:'left'
                        }
                    }}
                />
            </WrapperChart>
           
           
          

        </WrapperVote>

    )
}
export default Vote;