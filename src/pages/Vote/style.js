import styled from 'styled-components';

export const WrapperVote = styled.div`
    min-height:50vh;
    display:flex;
    flex-direction:column;
    padding:2rem;
    
`
export const WrapperTitle = styled.div`
    h1{
        font-size:4rem;
        font-weight:bold;
    }
`
export const WrapperButton = styled.div`
    display:flex;
    flex-direction:row;
    align-items:center;
    justify-content:flex-start;
    margin-top:2rem;
    margin-bottom:4rem;
`
export const ButtonItem = styled.div`
    display:flex;
    justify-content:center;
    align-items:center;
    background-color:white;
    width:200px;
    border:1px solid white;
    h2{
        color:black;
        font-weight:bold;
    }
    &:hover{
        cursor:pointer;
        background-color:#3E3F43;
        h2{
            color:white;
            font-weight:bold;
           
        }
        border:1px solid #3E3F43;

    }
    
    border-radius:4px;
    margin-right:2rem;
    margin-bottom:4rem;
    padding:1rem 1rem;
    
`
export const WrapperChart = styled.div`
    display:flex;
    justify-content:center;
    margin-bottom:4rem;
`