import React from 'react';
import {WrapperLayout, Navigation,Resume} from './style';
import About from '../About/index';
import Journey from '../Journey/index';
import Projects from '../Projects/index';
import Vote from '../Vote/index';
import {Helmet} from 'react-helmet';
import {Element, Link as ScrollLink} from 'react-scroll';
import ResumeIcon from '../../assets/img/resume.png';
function Container({...props}){
    return(
        <WrapperLayout>
            <Navigation>
                
                <ScrollLink to="about" spy={true} smooth={true} duration={500} className="header-item">
                    <h2>About</h2>         
                </ScrollLink>
                <ScrollLink to="vote" spy={true} smooth={true} duration={500} className="header-item">
                    <h2>Vote</h2>         
                </ScrollLink>
                <ScrollLink to="journey" spy={true} smooth={true} duration={500} className="header-item">
                    <h2>Journey</h2>         
                </ScrollLink>
                <ScrollLink to="projects" spy={true} smooth={true} duration={500} className="header-item">
                    <h2>Projects</h2>         
                </ScrollLink>
            </Navigation>
        
            <Resume>
                <a href="https://docs.google.com/document/d/1sbcdL1-qMUAcnRjOG8i-b37UG_QnlFddfOhTDAL5J0I/edit?usp=sharing" target="_blank" rel="noopener noreferrer">
                    <img src={ResumeIcon} alt="resume-icon"/> 

                </a>
             
               
            </Resume>

             <Helmet>
                <title>Muhamad Istiady Kartadibrata's Portofolio</title>
            </Helmet>
            <Element name="about"/>
            <About/>
            <Element name="vote"/>
            <Vote/>
            <Element name="journey"/>
            <Journey/>
            <Element name="projects"/>
            <Projects/>
           

        </WrapperLayout>
    )
}
export default Container;