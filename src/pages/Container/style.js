import styled from 'styled-components';

export const WrapperLayout = styled.div`
    background-color:#000000;
    min-height:100vh;
    overflow-x:hidden;
    overflow-y:hidden;

    position:relative;
    h1,h2,h3,h4,h5,h6,p,a{
        font-family: 'Nunito', sans-serif;
        color:white;
        font-weight:normal;
        margin:0;
        z-index:100;
    }
    padding:1rem 5rem;
    @media only screen and (max-width:768px){
        padding:1rem 1rem;
    }
    @media only screen and (max-width:450px){
       h1{
           font-size:1.5rem !important;
          
       }
       h2,h3,h4,h5,h6,p{
           font-size:0.8rem !important;
       }
    }
`
export const Navigation = styled.div`
    position:fixed;
    display:flex;
    z-index:10000000;
    left:2rem;
    flex-direction:column;
    a{
        z-index:10000000;
    }
    h2{
        z-index:10000000;
        font-weight:bolder;
        
      
        
        &:hover{
            cursor:pointer;
            animation: big 0.5s ;
            animation-fill-mode: forwards;
            @keyframes big{
                0%{
                    font-size:1rem;
                }
                100%{
                    font-size:2rem;
                }
            }
           
           
        }
        
        
        
    }

`
export const Resume = styled.div`
    position:fixed;
    width:75px;
    height:75px;
    border-radius:50%;
    background-color:white;
    bottom:3rem;
    z-index:10000;
    right:1rem;
    display:flex;
    justify-content:center;
    align-items:center;
    box-shadow: 1px 3px #888888;
    a{
        width:50%;
        &:hover{
            cursor:pointer;
            width:70%;
        }
        img{
            width:100%;
        }
    }
    
   


`