import React,{useState, useEffect} from 'react';
import {WrapperJourney, MaskRectangle, WrapperTitle ,Line, WrapperContent, WrapperBody, WrapperList, WrapperButton, ButtonItem} from './style';
import Flower from '../../assets/img/flower.png';
import Loading from '../../components/Loading';
import axios from 'axios';

function Journey({...props}){
    const [active, setActive] = useState('Education');
    const [isLoading, setIsLoading] = useState(true);
    const [ journey, setJourney] = useState({work:[], education:[], organization:[]});
    useEffect(()=>{
        setIsLoading(true)
        axios.get('https://portofolio-adykarta.herokuapp.com/api/journey',{headers:{'Access-Control-Allow-Origin': '*'}})
        .then((res)=>{
            setJourney(res.data)
            setIsLoading(false)
        })
    },[])
    useEffect(()=>{
        setIsLoading(true);
        setTimeout(()=>{
            setIsLoading(false)
        },2000)
    },[active])

    const handleChange = (param)=>{
        setActive(param)    
    }

    return(
        <WrapperJourney>
            <WrapperTitle>
                <h2>My Journey</h2>
                <Line/>
                <h1>EXPERIENCES</h1>
            </WrapperTitle>
            <img src={Flower} alt="flower"/>
            <MaskRectangle/>
            <WrapperContent>
                <h1 style={{fontWeight:'bold', marginBottom:'1rem'}}>{active}</h1>
                <WrapperBody loading={isLoading.toString()}>
                    {
                        isLoading ?

                        <Loading/>
                        :
                        <>
                            {
                                journey[active.toLowerCase()].map((el,idx)=>{
                                    return(
                                        <WrapperList key={idx}>
                                            <div className="name">
                                                <h4 style={{fontWeight:'bold'}}>{el.location}</h4>
                                            
                                                <h4 style={{fontWeight:'bold'}}>{el.time}</h4>
                                            </div>
                                            <div className="logo">
                                                <img src={el.logo} alt="logo" className="img-logo"/>
                                            </div>
                                            <div className="text">

                                                <h4 style={{fontWeight:'bold'}}>{el.name}</h4>
                                                <h4>{el.major}</h4>
                                                <h5>{el.text}</h5>
                                            </div>
                    
                                        </WrapperList>
                                    )
                                })
                            }
                        </>
                    }
                   
                </WrapperBody>
                <WrapperButton>
                    <ButtonItem active={active.toLowerCase()==='education'} onClick={()=>handleChange('Education')}>
                        <h3>Education</h3>
                    </ButtonItem>
                    <ButtonItem active={active.toLowerCase()==='work'} onClick={()=>handleChange('Work')}>
                        <h3>Work</h3>
                    </ButtonItem>
                    <ButtonItem active={active.toLowerCase()==='organization'} onClick={()=>handleChange('Organization')}>
                        <h3>Organization</h3>
                    </ButtonItem>

                </WrapperButton>
            </WrapperContent>
            
        </WrapperJourney>
    )
}
export default Journey;