import styled from 'styled-components';

export const WrapperJourney = styled.div`
    min-height:80vh;
    position:relative;
    img{
        position:absolute;
        top:18rem;
        left:-10rem;
        width:450px;
        z-index:1;
    }
`
export const MaskRectangle = styled.div`
    position:absolute;
    background-color:#C4C4C4;
    width:300px;
    height:400px;
    left:4rem;
    top:6rem;
    border:1px solid #C4C4C4;
    border-top-right-radius:30%;
    border-bottom-left-radius:30%;
    @media only screen and (max-width:1058px){
       top:10rem;
       left:-8rem;
    }
`
export const WrapperTitle = styled.div`
    h1{
        font-size: 4rem;
        font-weight:bolder;
    }
    h2{
        font-size:3rem;
        font-weight:bold;
    }
    display:flex;
    flex-direction:row;
    align-items:center;
    justify-content:flex-end;
    animation: inTitleJourney 1s ;
    animation-fill-mode: forwards;
    @keyframes inTitleJourney{
        0%{
            opacity:0;
        }
        100%{
            opacity:1;
        }
    }

    @media only screen and (max-width:1058px){
        flex-direction:column;
        justify-content:start;
    }
`
export const Line = styled.div`
    width:20%;
    height:3px;
    background-color:white;
    margin-left:1rem;
    margin-right:1rem;
`
export const WrapperContent = styled.div`
    display:flex;
    flex-direction:column;
    margin-left:30vw;
    @media only screen and (max-width:1058px){
        margin-left:10vw;
    }
    @media only screen and (max-width:600px){
        margin-left:0vw;
    }


`
export const WrapperBody = styled.div`
    width:100%;
    height:60vh;
    overflow-y:auto;
    border-bottom:10px solid white;
    border-right:10px solid white;
    ${({loading})=>loading==='true' && `

        display:flex;
        align-items:center;
        justify-content:center;
    `}
    z-index:1000;
`
export const WrapperList = styled.div`
    display:flex;
    flex-direction:row;
    margin-bottom:2rem;
    padding-right:0.5rem;
    .name{
        display:flex;
        flex-direction:column;
        width:30%;
    }
    .logo{
        z-index:10;
        width:10%;
        .img-logo{
            position:inherit;
            width:50px;
            
        }
    }
    .text{
        display:flex;
        flex-direction:column;
        width:60%;
    }
    @media only screen and (max-width:600px){
        flex-direction:column;
        .name{
            display:flex;
            flex-direction:column;
            width:100%;

        }
        .logo{
            z-index:10;
            width:100%;
            .img-logo{
                position:inherit;
                width:50px;
                
            }
        }
        .text{
            display:flex;
            flex-direction:column;
            width:100%;
        }

    }
`

export const WrapperButton = styled.div`
    display:flex;
    flex-direction:row;
    justify-content:start;
    margin-top:1rem;

`

export const ButtonItem = styled.div`
    display:flex;
    justify-content:center;
    align-items:center;
    background-color:${({active})=>active ?  '#FFFFFF' : '#313235'};
    border: 1px solid ${({active})=>active ?  '#FFFFFF' : '#313235'};
    border-top-right-radius:60%;
    border-bottom-left-radius:60%;
    width:150px;
    padding:1rem;
    margin-right:1rem;
    h3{
        font-weight:bold;
        color:${({active})=>active ?  '#313235' : '#FFFFFF'};
    }
    &:hover{
        cursor:pointer;
        background-color:#FFFFFF;
        h3{
            color:#313235;
        }
    }
    z-index:1000;
    
`