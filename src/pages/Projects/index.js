import React,{useState, useEffect} from 'react';
import {WrapperProjects,  WrapperTitle, Line, WrapperList, WrapperArrow} from './style';
import {ReactComponent as Arrow} from  '../../assets/icon/down-arrows.svg';
import Loading from '../../components/Loading';
import axios from 'axios';

function Projects({...props}){
    const [query, setQuery] = useState({
        skip:0,
    })
    const[loading, setLoading] = useState(false);
    const[projects, setProjects] =useState([]);

    useEffect(()=>{
        setLoading(true)
        axios.get('https://portofolio-adykarta.herokuapp.com/api/projects',{headers:{'Access-Control-Allow-Origin': '*'}})
        .then((res)=>{
            setProjects(res.data)
            setLoading(false)
        })
    },[])

    useEffect(()=>{
        setLoading(true);
        setTimeout(()=>{
            setLoading(false)
        },2000)
    },[query])

    const handleChange = (param)=>{
        if(param==='next'){
            setQuery({skip:query.skip+3})
        }
        else{
            setQuery({skip:query.skip-3})
        }
    }
   
    return(
        <WrapperProjects>
             <WrapperTitle>
                <h2>What i did</h2>
                <Line/>
                <h1>PROJECTS</h1>
            </WrapperTitle>
            {
                loading ?
                <div style={{height:'600px', display:'flex', alignItems:'center', justifyContent:'center'}}>
                    <Loading/>
                </div>
              
                :
                <WrapperList>
                {
                    projects.slice(query.skip,query.skip+3).map((el)=>{
                        return(         
                            <div className="flip-card" key={el.id}>
                                <div className="flip-card-inner">
                                    <div className="flip-card-front">
                                        <h1 style={{fontWeight:'bold'}}>{el.name.length>26 ? `${el.name.substring(0,26)}...` : el.name}</h1>
                                        <img src={el.image} alt={el.name}/>
                                    </div>
                                    <div className="flip-card-back">
                                        <h2>{el.description.length>200 ? `${el.description.substring(0,200)}...` : el.description}</h2>
                                        <a href={el.website} target="_blank" rel="noopener noreferrer" >
                                            <div className="see-more">
                                                <h4>Visit</h4>
                                            </div>     
                                        </a>       
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </WrapperList>

            }
            
            <WrapperArrow loading={loading.toString()}>
                <div style={query.skip-3<0 ?{visibility:'hidden'} : {visibility:"visible",display:'flex', alignItems:'center'}} onClick={()=>handleChange('back')}>
                    <Arrow className="back"/>
                    <h2 style={{fontWeight:'bold'}}>Back</h2>

                </div>
                <div style={query.skip+3>projects.length-1 ?{visibility:'hidden'} :{visibility:"visible",display:'flex', alignItems:'center'}} onClick={()=>handleChange('next')}>
                    <h2 style={{fontWeight:'bold'}}>Next</h2>
                    <Arrow className="next"/>

                </div>

            </WrapperArrow>
            <div className="big-round">

            </div>

         

        
        </WrapperProjects>
    )
}
export default Projects;