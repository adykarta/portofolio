import styled from 'styled-components';

export const WrapperProjects = styled.div`
    height:100vh;
    position:relative;
    .big-round{
        position:absolute;
        bottom:-30rem;
        height:900px;
        width:900px;
        border-radius:50%;
        background-color:#C4C4C4;
        left:-30rem;

    }
    @media only screen and (max-width:1200px){
        height:auto;
    }


`
export const WrapperTitle = styled.div`
    h1{
        font-size: 4rem;
        font-weight:bolder;
    }
    h2{
        font-size:3rem;
        font-weight:bold;
    }
    display:flex;
    flex-direction:row;
    align-items:center;
    justify-content:flex-start;
    animation: inTitleJourney 1s ;
    margin-top:3rem;
    animation-fill-mode: forwards;
    @keyframes inTitleJourney{
        0%{
            opacity:0;
        }
        100%{
            opacity:1;
        }
    }

    @media only screen and (max-width:1058px){
        flex-direction:column;
        justify-content:start;
      
    }

`
export const Line = styled.div`
    width:20%;
    height:3px;
    background-color:white;
    margin-left:1rem;
    margin-right:1rem;
`
export const WrapperList = styled.div`
    display:flex;
    flex-direction:row;
    align-items:center;
    justify-content:space-between;
    margin-top:3rem;
    z-index:1000;

    .flip-card {
        background-color: transparent;
        width:28%;
        height:450px;
        perspective: 1000px;
        z-index:1000;
      }
      
      .flip-card-inner {
        position: relative;
        width: 100%;
        height: 100%;
        text-align: left ;
        transition: transform 0.6s;
        transform-style: preserve-3d;
       
      }
      
      .flip-card:hover .flip-card-inner {
        transform: rotateY(180deg);
      }
      
      .flip-card-front, .flip-card-back {
        position: absolute;
        background-color:#3E3F43;
        padding-top:2rem;
        width: 100%;
        height: 100%;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        border:1px solid #3E3F43;
        border-top-right-radius:30%;
        border-bottom-left-radius:30%;
        padding-left:1rem;
        padding-right:1rem;
      }
      
      .flip-card-front {
     
        color: black;
      
      }
      .flip-card-front img{
        width:100%;
        
      }
      .flip-card-front h1{
        width:80%;
        height:20%;
        padding:0rem 2rem;
      }
      
      .flip-card-back {
        
        color: white;
        transform: rotateY(180deg);
      }
      .flip-card-back h2{
          padding:0rem 3rem;
      }
      .flip-card-back a{
          text-decoration:none;
      }
      .flip-card-back .see-more{
        margin-top:1rem;
        width:fit-content;
        padding:0.5rem 1rem;
        border:1px solid white;
        background-color:#3E3F43;
        border-radius:4px;
        margin-left:3rem;
        margin-top:2rem;
        &:hover{
            cursor:pointer;
            background-color:white;

            h4{
                color:#3E3F43;
            }
        }
    }
    @media only screen and (max-width:1200px){
        flex-direction:column;
        .flip-card{
            margin-bottom:4rem;
            width:50%;
            height:700px;
        }
    }
    @media only screen and (max-width:900px){
        .flip-card{
            margin-bottom:4rem;
            width:70%;
        }
    }
    @media only screen and (max-width:680px){
        .flip-card{
            margin-bottom:4rem;
            width:80%;
        }
    }
    @media only screen and (max-width:500px){
        .flip-card{
            margin-bottom:4rem;
            height:500px;
        }
    }
    @media only screen and (max-width:400px){
        .flip-card{
            margin-bottom:4rem;
            height:400px;
        }
    }
`
export const WrapperMask = styled.div`
    width:28%;
    height:450px;
    perspective: 1000px;
`
export const MaskRectangle = styled.div`
    
    display:flex;
    flex-direction:column;
    background-color:#3E3F43;
    position: relative;
    width:100%;
    height:100%;
    border:1px solid #3E3F43;
    border-top-right-radius:30%;
    border-bottom-left-radius:30%;
    transition: transform 0.6s;
    transform-style: preserve-3d;
   

    .front{
        position: absolute;
        width: 100%;
        height: 100%;
        -webkit-backface-visibility:visible;
        backface-visibility: visible;
    }
    .back{
        position: absolute;
        width: 100%;
        height: 100%;
        -webkit-backface-visibility: visible;
        backface-visibility: visible;
        transform: rotateY(180deg);
    }
    ${({active})=>active && `
        transform: rotateY(180deg);
    `}
    padding:1rem;
    h1{
        width:80%;
        height:100px;
    }
    img{
        width:100%;
    }
    .see-more{
        margin-top:1rem;
        width:fit-content;
        padding:0.5rem 1rem;
        border:1px solid white;
        background-color:#3E3F43;
        border-radius:4px;
        z-index:1000;
        &:hover{
            cursor:pointer;
            background-color:white;

            h4{
                color:#3E3F43;
            }
        }
    }
   
   
`

export const WrapperArrow = styled.div`
    display:${({loading})=>loading==='loading' ? 'none' :'flex'};
    flex-direction:row;
    margin-top:4rem;
    align-items:center;
    justify-content:space-between;
    z-index:1000;
    .next{
        transform: rotateZ(-90deg);
       
        margin-left:1rem;
        z-index:1000;
    }
    .back{
        transform: rotateZ(-270deg);
       
        margin-right:1rem;
        z-index:1000;

    }
    div{
        &:hover{
            cursor:pointer;
        }
        z-index:1000;
    }
    
`